   /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *  * /*Nombre de la clase: Persona
 *Fecha: 15 - 03 - 2021
 * Version: 1.0
 * @author Daniel Omar Tijaro Beltran 
 * Codigo: 1151859
 * Estructuras de datos - C
 * Requisitos:
 *      1. Un objeto es igual a otro si tienen el mismo valor para todos sus parciales.
 *      2. Un objeto es >,< con el análisis del promedio de sus notas según nuestro reglamento
  
 * @author madar
 */
public class Estudiante {
    
    private long codigo;
    private float p1, p2,p3, examen;
    private Object other;
    
    //Requisito 1

    public Estudiante() {
    }

    public Estudiante(long codigo, float p1, float p2, float p3, float examen) {
        this.codigo = codigo;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.examen = examen;
    }
    //Requisito 2

    public long getCodigo() {
        return codigo;
    }

    public float getP1() {
        return p1;
    }

    public float getP2() {
        return p2;
    }

    public float getP3() {
        return p3;
    }

    public float getExamen() {
        return examen;                  
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public void setP1(float p1) {
        this.p1 = p1;
    }

    public void setP2(float p2) {
        this.p2 = p2;
    }

    public void setP3(float p3) {
        this.p3 = p3;
    }

    public void setExamen(float examen) {
        this.examen = examen;
    }


    
    //Requisito 3 

    @Override
    public String toString() {
        return "Estudiante{" + "codigo=" + codigo + ", p1=" + p1 + ", p2=" + p2 + ", p3=" + p3 + ", examen=" + examen + '}';
    }
    
    
    //Requisito 4

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (int) (this.codigo ^ (this.codigo >>> 32));
        hash = 79 * hash + Float.floatToIntBits(this.p1);
        hash = 79 * hash + Float.floatToIntBits(this.p2);
        hash = 79 * hash + Float.floatToIntBits(this.p3);
        hash = 79 * hash + Float.floatToIntBits(this.examen);
        return hash;
    }
    
    
    //El metodo equals viene de la clase object por tanto su parametro es un objeto
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estudiante other = (Estudiante) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        if (Float.floatToIntBits(this.p1) != Float.floatToIntBits(other.p1)) {
            return false;
        }
        if (Float.floatToIntBits(this.p2) != Float.floatToIntBits(other.p2)) {
            return false;
        }
        if (Float.floatToIntBits(this.p3) != Float.floatToIntBits(other.p3)) {
            return false;
        }
        if (Float.floatToIntBits(this.examen) != Float.floatToIntBits(other.examen)) {
            return false;
        }
        return true;
    }
    
    public float getCalcularPromedio(){
        float promedio=0;
        promedio = (float) ((((this.p1+this.p2+this.p3)/3)*0.7)+(this.examen*0.3));
        return promedio;
} 
    public int compareTo(Object obj){
        int valor=1;
        final Estudiante other = (Estudiante) obj;
        
        if(this.equals(obj)){
            valor=0;
       }
       
        if(this.getCalcularPromedio()>other.getCalcularPromedio()){
           valor=1;
        } 
        
        if(this.getCalcularPromedio()-other.getCalcularPromedio()<0){
           valor=-1;
        }
        return valor;
    }

        
    
    
}
