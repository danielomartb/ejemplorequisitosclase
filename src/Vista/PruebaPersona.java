/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Persona;

/**
 *
 * @author madar
 */
public class PruebaPersona {
    public static void main(String[] args) {
        Persona x=new Persona(111,"jorge");
        Persona y=new Persona(111,"astrid");
        
        //Imprimir los objetos:
        System.out.println("X:"+x.toString());
        System.out.println("Y:"+y.toString());
        
        //Comparando si son iguales por sus cédulas:
        if(x.equals(y))
            System.out.println(":) SI son iguales por su cédula");
        else
            System.out.println(":( NO son iguales por su cédula");
        
        //Comparar:
        int comparador=x.compareTo(y);
        
        String mensaje="Son iguales";
        switch(comparador)
        {
            
            case 1:{
                mensaje="EL objeto 1 es mayor al objeto 2";
                break;
            }
            case -1:{
                mensaje="EL objeto 1 es menor al objeto 2";
                break;
            }
        }
        System.out.println("Valor del comparador:"+comparador+","+mensaje);
    }
    
}
