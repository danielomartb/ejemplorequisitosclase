/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import java.util.Scanner;

/**
 * /*Nombre de la clase: Persona
 *Fecha: 15 - 03 - 2021
 * Version: 1.0
 * @author Daniel Omar Tijaro Beltran 
 * Codigo: 1151859
 * Estructuras de datos - C
 *  SU TAREA CONSISTE EN IMPLEMENTAR LOS REQUISITOS DE UNA CLASE DE OBJETO DE TIPO ESTUDIANTE
 * DEBE PROBARLO CON 5 OBJETOS DIFERENTES Y REALIZAR COMPARACIONES ENTRE ELLOS
 * @author madar
 */
public class PruebaEstudiante {
    public static void main(String[]args){
    Estudiante a = new  Estudiante(0, 3, 2, 2, 4);//Inicialice objetos de ejemplo, los 5 que me solicitaban usar para hacer comparaciones entre ellos.
    Estudiante b = new  Estudiante(3, 3, 1, 1, 5);
    Estudiante c = new  Estudiante(0, 3, 2, 2, 4);
    Estudiante d = new  Estudiante(5, 4, 3, 1, 2);
    Estudiante e = new  Estudiante(3, 3, 1, 1, 5);
    Scanner sc = new Scanner(System.in);
    
        
        Estudiante aux1= new Estudiante(0, 0, 0, 0, 0);//Creé dos objetos de tipo Estudiante para guardar los objetos seleccionados por el usuario.
        Estudiante aux2= new Estudiante(0, 0, 0, 0, 0);
        System.out.println("1).a: "+a.toString());//Imprimí los 5 objetos de tipo estudiante en formato de cadena con el metodo toString de la clase estudiante.
        System.out.println("2).b: "+b.toString());
        System.out.println("3).c: "+c.toString());
        System.out.println("4).d: "+d.toString());
        System.out.println("5).e: "+e.toString()+"\n");
        System.out.println("Seleccione el numero del primer estudiante a comparar, seguido del segundo estudiante a comparar\n");
        int estudiante1 = sc.nextInt();//Creé dos variables de tipo entero como auxiliares para determinar los dos estudiantes a comparar.
        int estudiante2 = sc.nextInt();
        
        if(estudiante1==1)aux1=a;
         if(estudiante1==2)aux1=b;//Guardé el primero estudiante a comparar en otro objeto.
          if(estudiante1==3)aux1=c;
           if(estudiante1==4)aux1=d;
            if(estudiante1==5)aux1=e;
        
        if(estudiante2==1)aux2=a;
         if(estudiante2==2)aux2=b;//Guardé el segundo objeto a comparar en otro objeto dependiendo de la opcion 
          if(estudiante2==3)aux2=c;//escogida como segundo estudiante a comparar.
           if(estudiante2==4)aux2=d;
            if(estudiante2==5)aux2=e;
            
        System.out.println(""+estudiante1+": "+aux1.toString()+"  Promedio1:"+" "+aux1.getCalcularPromedio());//Imprimí como una cadena los datos de los dos estudiantes
        System.out.println(""+estudiante2+": "+aux2.toString()+"  Promedio2:"+" "+aux2.getCalcularPromedio()+"\n");//seleccionados usando el metodo toString de la clase Estudiante.
           
        
         if(aux1.equals(aux2))//Comparé si todos los parciales de ambos estudiantes eran iguales con el metodo equals de la clase persona.
            System.out.println(" SI son iguales porque el valor de todos sus parciales es igual\n");
        else
            System.out.println(" NO son iguales porque el valor de todos sus parciales no es igual\n");
        
        //Comparar:
        int comparador= aux1.compareTo(aux2);//Comparé los dos objetos elegidos por el usuario
        //                                    guardados en los auxiliares con el metodo compareTo de la clase Estudiante.
       
        String mensaje="Son iguales\n";
        switch(comparador)
        {
            case 1:{
                mensaje="EL objeto 1 es mayor al objeto 2 porque el promedio de sus parciales es mayor\n";
                break;
            }
            case -1:{
                mensaje="EL objeto 1 es menor al objeto 2 porque el promedio de sus parciales es menor\n";
                break;
            }
        }
        System.out.println("Valor del comparador:"+comparador+","+mensaje);
    }
}
